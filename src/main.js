import Vue from "vue";
import App from "./App";
import '../node_modules/bulma/css/bulma.css';

new Vue({
  el: "#app",
  render: h => h(App)
});
